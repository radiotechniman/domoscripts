#!/bin/bash
#This query removes all meter values from the 'OTGW_...' meters, only those older than 60 days
SQL="DELETE FROM metrics WHERE datetime < DATE_SUB(CURDATE(),INTERVAL 60 DAY) AND meter_id IN (16,17,18,19,22,24,25,26,27,28,29,32,33)"

MYSQL_USER="username"
MYSQL_PASS="usernamepass"
MYSQL_DB="username"

echo $SQL | /usr/bin/mysql --user=$MYSQL_USER --password=$MYSQL_PASS $MYSQL_DB