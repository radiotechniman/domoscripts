-- script_device_season.lua
-- This script allows you to manage the value of a user variable depending on this season:
-- Spring Summer Fall Winter
-- The change of state is a fixed date:
-- Lente: 1 maart t/m 31 mei
-- Zomer: 1 juni t/m 31 augustus
-- Herfst: 1 september t/m 30 november
-- Winter: 1 december t/m 28 februari
-- The script is run once a day when the virtual switch 'Day' gets flipped (every sunset & sunrise)
commandArray = {}

time = os.time()
month = tonumber(os.date('%m', time))
day = tonumber(os.date('%d', time))

if devicechanged['IsDonker (virt)'] == 'On' then
   if month == 03 and day >= 21 then 
      print('Het seizoen is veranderd, het is nu lente')
      commandArray['Variable:Seizoen'] = 'Lente'
      commandArray['OpenURL'] = "http://127.0.0.1:80/json.htm?type=command&param=udevice&idx=253&nvalue=0&svalue=Lente"
   end
   if month == 06 and day >= 21 then 
      print('Het seizoen is veranderd, het is nu zomer')
      commandArray['Variable:Seizoen'] = 'Zomer'
      commandArray['OpenURL'] = "http://127.0.0.1:80/json.htm?type=command&param=udevice&idx=253&nvalue=0&svalue=Zomer"
   end
   if month == 09 and day >= 21 then 
      print('Het seizoen is veranderd, het is nu herfst')
      commandArray['Variable:Seizoen'] = 'Herfst'
      commandArray['OpenURL'] = "http://127.0.0.1:80/json.htm?type=command&param=udevice&idx=253&nvalue=0&svalue=Herfst"
   end
   if month == 12 and day >= 21 then 
      print('Het seizoen is veranderd, het is nu lente')      
      commandArray['Variable:Seizoen'] = 'Winter'
      commandArray['OpenURL'] = "http://127.0.0.1:80/json.htm?type=command&param=udevice&idx=253&nvalue=0&svalue=Winter"
   end
end

return commandArray