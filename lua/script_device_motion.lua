-- script_time_motion.lua
local motion_switch = 'Motion'
local status_switch = 'IemandThuis'
local hallway_switch = 'Licht gang'

commandArray = {}

if (devicechanged[motion_switch] == 'On' and otherdevices[status_switch]) == 'Off' then --motion detected, so there is someone home
 	commandArray[status_switch]='On'

 	if (otherdevices[hallway_switch] == 'On') then --turn off the light in the hallway, when we enter the livingroom
 	commandArray[hallway_switch]='Off'
	end

end
return commandArray



